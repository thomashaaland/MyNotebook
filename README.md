# My Notepad

This is a notepad project. It allows you to write, cut, copy and paste text. It also allows you to save and load projects. It requires [Qt](qt.io).
